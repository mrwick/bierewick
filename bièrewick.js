// credentials not included in repository
import { cred } from './untappd_cred.js';
log('bièrewick cred imported');

document.title = "bièrewick";
log('bièrewick title set');

let b = document.body;
let d0 = document.createElement("div");
let checkins_access_token = cred.checkins_access_token;
let checkins_url = "https://api.untappd.com/v4/user/checkins?";
checkins_url += "access_token=" + checkins_access_token;
log('bièrewick variables set');

function log(text) {
  console.log('logmsg: ' + text + "\n");
}

function wrapboldbr(t) {
  let f = document.createDocumentFragment();
  let b = document.createElement("b");
  let br = document.createElement("br");
  b.innerText = t;
  f.append(b, br);

  return f
  // "<b>" + t + "</b><br/>\n";
}

function ahref(h, t) {
  // href, optional text
  let a = document.createElement("a");
  if (!t) t = h;

  a.href = h;
  a.innerHTML = t;
  return a;
  // '<a href=' + h +'>' + t + "</a>\n";
}
log('bièrewick utiilties declared');

function next_checkins(next_url) {
  let b = document.createElement("button");
  b.innerText = 'NEXT';
  b.type = 'button';

  next_url += '&access_token=';
  next_url += cred.checkins_access_token;
  next_url += '&client_id=' + cred.client_id;
  next_url += '&client_secret=' + cred.client_secret;

  b.onclick = function next_click(e) {
    log('click down');
    fetch_checkins(next_url);
  }

  return b;
}

log('bièrewick content create start');
function fetch_checkins(url) {
  fetch(url,
    {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Origin': 'https://freeshell.de'}
    }).
    then(response => response.json()).
    then(json => checkins(json));
}

fetch_checkins(checkins_url);

function checkins(checkins_json) {
  let o = checkins_json.response.checkins.items;
  for (let x = 0; x < o.length; x++) {
    let p = document.createElement('p');
    p.id = 'bier' + x;
    if (o[x].created_at) {
      p.appendChild(wrapboldbr(o[x].created_at));
    }
    p.appendChild(wrapboldbr(o[x].beer.beer_name));
    p.appendChild(wrapboldbr(o[x].brewery.brewery_name));
    if (o[x].venue.venue_name) {
      p.appendChild(wrapboldbr(o[x].venue.venue_name));
    }
    d0.append(p);
  }
  let nu = checkins_json.response.pagination.next_url;
log('next_url:'+ nu);
  d0.append(next_checkins(nu));
}
log('bièrewick content create end');

log('bièrewick append div');
b.append(d0);
